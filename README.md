Questa è una libreria PHP creata con lo scopo di rendere più facile la creazione
di codice HTML.

Nota bene: CREARE, non manipolare. Per quello c'è già la manipolazione dei DOM 
di PHP.
Infatti queste classi hanno il solo scopo di automatizzare\facilitare la 
generazione stringhe di codice HTML corretto, rendendo più leggibile il codice 
PHP.

Una grossa parte dei tag HTML ha una serie di proprietà comuni: queste formano 
le proprietà della class-base htmlTagGeneric, di cui quasi tutte le altre classi
son estensioni, con i loro valori di default e le loro proprie
    tà specifiche.

l'idea base di funzionamento è (usando il tag DV come esempio):

```
$tagDiv = htmlTagDiv();
$tagDiv->id = "Nome Div";
$tagDiv->onBlur = "sottolinea()";
$tagDiv->content = "Tutto il contenuto di questo DIV viene sottolineato quando ci si passa sopra";
echo $tagDiv;
```


risulta in
```
<div
    nome="Nome Div"
    onClic="sottolinea()"
>Tutto il contenuto di questo DIV viene sottolineato quando ci si passa sopra</div>
```

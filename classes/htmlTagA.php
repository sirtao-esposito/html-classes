<?php



/**
 * htmlTagA.
 *
 * @author	Unknown
 * @author	Sirtao Esposito
 * @since	v0.0.1
 * @version	v1.0.0	Monday, April 15th, 2019.	
 * @version	v1.0.1	Monday, April 15th, 2019.
 * @see		htmlTagGeneric
 * @global
 */
class htmlTagA extends htmlTagGeneric{
    protected $startTagOpen = "<a";
    protected $startTagClose = ">";
    protected $endTag = "</a>";

    //private $content="";


    public $download = "";

    public $href = "";
    public $hreflang = "";
    public $media = "";
    public $ping = "";
    public $rel = "";
    public $target = "";
    public $type = "";

  
    /**
     * Set the value of target
     * Opzionale.
     * valori accettati:
     * _blank
     * _parent
     * _self
     * _top
     * nomeDelFrame
     *
     * @author	Unknown
     * @since	v0.0.1
     * @version	v1.0.0	Saturday, April 6th, 2019.	
     * @version	v1.0.1	Saturday, April 6th, 2019.	
     * @version	v1.0.2	Saturday, April 6th, 2019.	
     * @version	v1.0.3	Saturday, April 6th, 2019.
     * @access	public
     * @param	string	$target	
     * @return	self
     */
    public function setTarget(string $target){
        $this->target = $target;

        return $this;
    }

    /**
     * Set the value of rel
     * Opzionale.
     * Valori accettati:
     * alternate
     * author
     * bookmark
     * external
     * help
     * license
     * next
     * nofollow
     * noreferrer
     * noopener
     * prev
     * search
     * tag
     * 
     * @since	v0.0.1
     * @version	v1.0.0	Saturday, April 6th, 2019.	
     * @version	v1.0.1	Saturday, April 6th, 2019.
     * @access	public
     * @param	string	$rel	
     * @return	self
     */
    public function setRel(String $rel){
        $acceptedValues = [
            "alternate",
            "author",
            "bookmark",
            "external",
            "help",
            "license",
            "next",
            "nofollow",
            "noreferrer",
            "noopener",
            "prev",
            "search",
            "tag"
        ];

        try {
            if (in_array($rel, $$acceptedValues)) {
                $this->rel = $rel;
                return $this;
            } else {
                $messaggio="ERRRORE: inserito valore '".$rel."', ma gli unici valori accettati sono:\n";
                foreach ($acceptedValues as $key) {
                    $messaggio .= $key."\n";
                }
                throw new Error($messaggio);
            }
        } catch(Error $e){
            echo $e;
        }

    }

    /**
     * Set the value of href
     * il valore _NON_ viene sanitizzato automaticamente
     * è quindi necessario sanitizzarlo a monte
     * @author	Unknown
     * @since	v0.0.1
     * @version	v1.0.0	Saturday, April 6th, 2019.
     * @access	public
     * @param	string	$href	DA SANITIZARE A MONTE
     * @return	self
     */
    public function setHref(string $href)
    {
        $this->href = $href;
        //return $this;
    }
}
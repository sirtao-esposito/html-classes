<?php
/**
 * require_once "htmlTagGeneric.php";
 * Undocumented class
 *
 * @author	Sirtao Esposito
 * @since	v0.0.1
 * @version	v1.0.0	Monday, April 15th, 2019.
 * @see		htmlTagGeneric
 * @global
 */
class htmlTagDiv extends htmlTagGeneric
{
    protected $startTagOpen = "<div";
    protected $startTagClose = ">";
    protected $endTag = "</div>";

}

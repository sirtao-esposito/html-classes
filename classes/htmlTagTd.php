<?php

/**
 * htmlTagTd.
 *
 * @author	Sirtao Esposito
 * @since	v0.0.1
 * @version	v1.0.0	Monday, April 15th, 2019.
 * @see		htmlTagGeneric
 * @global
 */
class htmlTagTd extends htmlTagGeneric
{
    protected $startTagOpen = "<td";
    protected $startTagClose = ">";
    protected $endTag = "</td>";
}
<?php
//require_once "htmlTagGeneric.php";

/**
 * htmlTagTh.
 *
 * @author	Sirtao Esposito
 * @since	v0.0.1
 * @version	v1.0.0	Monday, April 15th, 2019.
 * @see		htmlTagGeneric
 * @global
 */
class htmlTagTh extends htmlTagGeneric
{
    protected $startTagOpen = "<th";
    protected $startTagClose = ">";
    protected $endTag = "</th>";
}

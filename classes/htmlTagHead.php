<?php
/**
 * Undocumented class
 */
class htmlTagHead extends htmlTagGeneric
{
    protected $startTagOpen = "<head";
    protected $startTagClose = ">";
    protected $endTag = "</head>";
}
<?php
/**
 * Undocumented class
 */
class htmlTagHtml extends htmlTagGeneric
{
    private $xmlns = false;

    protected $startTagOpen = "<html";
    protected $startTagClose = ">";
    protected $endTag = "</html>";


    /**
     * Whether specify the XML namespace attribute (If you need your content to conform to XHTML)
     * Namespace attribute is "http://www.w3.org/1999/xhtml"
     * If not specified, it's FALSE by Class Default.
     *
     * @author	Sirtao Esposito
     * @since	v0.0.1
     * @version	v1.0.0	Monday, April 15th, 2019.
     * @version	v1.0.1	Monday, April 15th, 2019.
     * @access	public
     * @param	bool	$value	Set to TRUE to specify the XML namespace attribute
     * @return	void
     */
    public function setXmlns(bool $value)
    {
        $this->xmlns = $value;
    }
}

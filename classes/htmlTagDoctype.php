<?php
/**
 * Undocumented class
 *
 * @author	Sirtao Esposito
 * @since	v0.0.1
 * @version	v1.0.0	Monday, April 15th, 2019.
 * @global
 */
class htmlTagDoctype {
    /**
     * @var		mixed	$version
     */
    protected $version;

    /**
     * Undocumented function
     *
     * @author	Sirtao Esposito
     * @since	v0.0.1
     * @version	v1.0.0	Monday, April 15th, 2019.
     * @access	public
     * @return	mixed
     */
    public function __toString()
    {
        return $this->version;
    }

    /**
     * Undocumented function
     *
     * @author	Sirtao Esposito
     * @since	v0.0.1
     * @version	v1.0.0	Monday, April 15th, 2019.	
     * @version	v1.0.1	Monday, April 15th, 2019.	
     * @version	v1.0.2	Monday, April 15th, 2019.
     * @access	public
     * @param	string	$version	Default: "h5"
     * @return	void
     */
    public function __construct(string $version = "h5")
    {
        try {

            switch (strtolower($version)) {
                case "h5":
                    $this->version = '<!DOCTYPE html>';
                    break;

                case "h4t":
                    $this->version = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
                    break;

                case "h4s":
                    $this->version = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">';
                    break;

                case "h4f":
                    $this->version = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">';
                    break;

                case "x1t":
                    $this->version = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
                    break;

                case "x1s":
                    $this->version = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
                    break;

                case "x1f":
                    $this->version = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">';
                    break;

                case "x11":
                    $this->version = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">';
                    break;

                default:
                    throw new Error;
            }
        } catch (Error $e) {
            echo "doctype errato, scegliere tra uno dei seguenti:\n";
            echo "h5 per HTML5 (è anche il default se non ne viene scelto nessuno)\n";
            echo "h4s per HTML 4.01 Strict\n";
            echo "h4t per HTML 4.01 Transitional\n";
            echo "h4f per HTML 4.01 Frameset\n";
            echo "x1s per XHTML 1.0 Strict\n";
            echo "x1t per XHTML 1.0 Transitional\n";
            echo "x1f per XHTML 1.0 Frameset\n";
            echo "x11 per XHTML 1.1\n";
        }
    }
}

<?php


/**
 * htmlTagTitle.
 *
 * @author	Sirtao Esposito
 * @since	v0.0.1
 * @version	v1.0.0	Monday, April 15th, 2019.
 * @see		htmlTagGeneric
 * @global
 */
class htmlTagTitle extends htmlTagGeneric
{
    protected $startTagOpen = "<title";
    protected $startTagClose = ">";
    protected $endTag = "</title>";

}

<?php
/**
 * Undocumented class
 *
 * @author	Sirtao Esposito
 * @since	v0.0.1
 * @version	v1.0.0	Monday, April 15th, 2019.
 * @see		htmlTagGeneric
 * @global
 */
class htmlTagBody extends htmlTagGeneric
{

    /** funzioni non più supportate in html5 */
    /*
        private $alink;
        private $background;
        private $bgcolor;
        private $link;
        private $text;
        private $vlink;
    */

    protected $startTagOpen = "<body";
    protected $startTagClose = ">";
    protected $endTag = "</body>";
}

<?php

/**
 * Classe htmlTagGeneric.
 *
 * @author	Unknown
 * @author	Sirtao Esposito
 * @since	v0.0.1
 * @version	v1.0.0	Monday, April 15th, 2019.	
 * @version	v1.0.1	Monday, April 15th, 2019.	
 * @version	v1.0.2	Monday, April 15th, 2019.
 * @global
 */
class htmlTagGeneric
{
    public $accesskey = "";
    protected $class = [];
    public $contenteditable = "";
    protected $data = [];
    public $dir = "";
    public $draggable = "";
    public $dropzone = "";
    public $hidden = "";
    public $id = "";
    public $lang = "";
    public $spellcheck = "";
    public $style = "";
    public $tabindex = "";
    public $title = "";
    public $translate = "";

    protected $startTagOpen = "";
    protected $startTagClose = "";
    protected $endTag = "";
    public $content = "";

    public $spazi = "  ";

    /**
     * [ErroreTipo description]
     *
     * @author	Sirtao Esposito
     * @since	v0.0.1
     * @version	v1.0.0	Monday, April 15th, 2019.	
     * @version	v1.0.1	Monday, April 15th, 2019.	
     * @version	v1.0.2	Monday, April 15th, 2019.	
     * @version	v1.0.3	Monday, April 15th, 2019.
     * @access	protected
     * @param	string	$valorePassato	[$valorePassato description]
     * @param	array 	$tipiAccettati	[$tipiAccettati description]
     * @return	void
     */
    protected function ErroreTipo(string $valorePassato, array $tipiAccettati): void
    {
        echo "è stato valorePassato un tipo non " . $tipiAccettati . "\n";
        echo "tipo di dato " . $tipiAccettati . ":";
        foreach ($tipiAccettati as $tipo) {
            echo " " . $tipo . ",";
        }
        echo "\n";
        echo "tipo di dato ricevuto: " . $valorePassato . "\n";
    }





    
    /**
     * setData.
     *
     * @author	Sirtao Esposito
     * @since	v0.0.1
     * @version	v1.0.0	Monday, April 15th, 2019.
     * @access	public
     * @param	string	$key  	
     * @param	string	$value	
     * @return	void
     */
    public function setData(string $key, string $value)
    {
        $this->data[$key] = $value;
    }


    /**
     * [getData description]
     *
     * @author	Sirtao Esposito
     * @since	v0.0.1
     * @version	v1.0.0	Monday, April 15th, 2019.	
     * @version	v1.0.1	Monday, April 15th, 2019.
     * @access	public
     * @return	mixed
     */
    public function getData():string
    {
        return print_R($this->data);
    }








    /**
     * Get the value of class
     *
     * @author	Sirtao Esposito
     * @since	v0.0.1
     * @version	v1.0.0	Monday, April 15th, 2019.
     * @access	public
     * @return	mixed
     */
    public function getClass(): string
    {
        return print_R($this->class);
    }

    /**
     * Alias di addClass.
     * Deprecato
     * è preferibile usare addClass per motivi sintattici
     *
     * @author	Sirtao Esposito
     * @since	v0.0.1
     * @version	v1.0.0	Monday, April 15th, 2019.	
     * @version	v1.0.1	Monday, April 15th, 2019.
     * @deprecated v1.0.1
     * @access	public
     * @param	string	$class	
     * @see     htmlTagGeneric::addClass()  per aggiungere classi
     * @return	void
     */
    public function setClass(string $class): void
    {
        $this->addClass($class);
    } 
    



    /**
     * addClass.
     * è preferibile per motivi sintattici.
     *
     * @author	Sirtao Esposito
     * @since	v0.0.1
     * @version	v1.0.0	Monday, April 15th, 2019.	
     * @version	v1.0.1	Monday, April 15th, 2019.
     * @access	public
     * @param	string	$class	
     * @return	void
     */
    public function addClass(string $class): void
    {
        array_push($this->class, $class);
    }

    

    /**
     * __set.
     *
     * @author	Sirtao Esposito
     * @since	v0.0.1
     * @version	v1.0.0	Monday, April 15th, 2019.
     * @access	public
     * @param	string	$name 	
     * @param	string	$value	
     * @return	void
     */
    public function __set(string $name, string $value): void
    {
            throw new Exception('Property "' . $name . '" does not exist');   
    }









    /**
     * [__toString description]
     *
     * @author	Unknown
     * @since	v0.0.1
     * @version	v1.0.0	Monday, April 15th, 2019.
     * @access	public
     * @return	void
     */
    public function __toString()
    {
        $attributes = null;
        $spazi=$this->spazi .$this->spazi;
        foreach ($this as $key => $value) {
            if ($value != null) {
                switch ($key) {
                    case "startTagOpen":
                    case "startTagClose":
                    case "endTag":
                    case "content":
                    case "spazi":
                        break;
                    case "data":
                        foreach ($value as $keyData => $valueData) {
                            $attributes .= "\n" . $spazi . "data-" . $keyData . "='" . $valueData . "'";
                        }
                        break;
                    case "class":
                        $attributes .= "\n" . $spazi . "class='" . implode(" ", $value) . "'";
                        break;
                    default:
                        $attributes .= "\n" . $spazi . $key . "='" . $value . "'";
                }
            }
        }
        $return = 
            "\n" . $this->spazi . $this->startTagOpen .
            (is_null($attributes) ? "": $attributes ."\n" . $this->spazi ) .
            $this->startTagClose .
            $this->content .
            $this->endTag . "\n";
        return $return;
    }




}
